﻿using Accord.Imaging;
using Accord.Vision.Detection;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;

namespace GenderRecognition.Data.FaceExtracting
{
    public class FaceExtractor : IFaceExtractor
    {
        private static readonly Rectangle EmptyRectangle = new Rectangle();

        private readonly IObjectDetector objectDetector;

        public FaceExtractor(IObjectDetector objectDetector)
        {
            this.objectDetector = objectDetector;
        }

        public List<ImageData> ExtractFaces(IEnumerable<ImageData> images)
        {
            return images
                .Select(ExtractFace)
                .Where(image => image != null)
                .ToList();
        }

        private ImageData ExtractFace(ImageData imageData)
        {
            var faceRectangle = GetFaceRectangle(imageData.Bitmap);

            if (faceRectangle == EmptyRectangle)
                return null;

            var croppedImage = CropBitmap(imageData.Bitmap, faceRectangle);

            return new ImageData(croppedImage, imageData.Gender);
        }

        private Rectangle GetFaceRectangle(Bitmap bitmap)
        {
            var detectedObjects = objectDetector
               .ProcessFrame(UnmanagedImage.FromManagedImage(bitmap))
               .ToList();

            if (detectedObjects.Count != 1)
                return EmptyRectangle;

            return detectedObjects.Single();
        }

        private Bitmap CropBitmap(Bitmap bitmap, Rectangle cropRectangle)
        {
            return bitmap.Clone(cropRectangle, bitmap.PixelFormat);
        }
    }
}
