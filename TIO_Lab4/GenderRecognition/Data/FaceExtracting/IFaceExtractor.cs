﻿using System.Collections.Generic;

namespace GenderRecognition.Data.FaceExtracting
{
    public interface IFaceExtractor
    {
        List<ImageData> ExtractFaces(IEnumerable<ImageData> images);
    }
}
