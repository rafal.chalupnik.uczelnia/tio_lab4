﻿using System;
using System.Drawing;

namespace GenderRecognition.Data
{
    public class ImageData
    {
        public Bitmap Bitmap { get; }

        public Gender Gender { get; }

        public ImageData(Bitmap bitmap, Gender gender)
        {
            Bitmap = bitmap;// ?? throw new ArgumentException("Image cannot be null");
            Gender = gender;
        }
    }
}
