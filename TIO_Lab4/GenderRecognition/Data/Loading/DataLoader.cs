﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;

namespace GenderRecognition.Data.Loading
{
    public class DataLoader : IDataLoader
    {
        public List<ImageData> LoadImages(string directoryPath)
        {
            if (!Directory.Exists(directoryPath))
                throw new ArgumentException("Provided directory doesn't exist.");

            return Directory.GetFiles(directoryPath)
                .Select(ReadImage)
                .ToList();
        }

        private static ImageData ReadImage(string filePath)
        {
            var bitmap = new Bitmap(Image.FromFile(filePath));
            var gender = GetGenderFromFilePath(filePath);

            return new ImageData(bitmap, gender);
        }

        private static Gender GetGenderFromFilePath(string filePath)
        {
            var genderIntString = Path.GetFileName(filePath)
                .Split('_', StringSplitOptions.RemoveEmptyEntries)
                .Skip(1) // Age
                .Take(1) // Gender
                .Single();

            var isGenderInt = int.TryParse(genderIntString, out var genderInt);

            if (!isGenderInt)
                throw new InvalidCastException($"Gender in file name must be int. File path: {filePath}");

            if (genderInt < 0 || genderInt > 1)
                throw new InvalidCastException($"Gender must be either 0 or 1. File path: {filePath}");

            return (Gender) genderInt;
        }
    }
}
