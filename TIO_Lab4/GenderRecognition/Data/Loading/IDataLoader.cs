﻿using System.Collections.Generic;

namespace GenderRecognition.Data.Loading
{
    public interface IDataLoader
    {
        List<ImageData> LoadImages(string directory);
    }
}
