﻿using System;
using GenderRecognition.Data;

namespace GenderRecognition.FeatureExtraction
{
    public class FeatureData
    {
        public Gender Gender { get; }

        public double[] Features { get; }

        public FeatureData(double[] features, Gender gender)
        {
            if (features == null)
                throw new ArgumentException("Features cannot be null.");

            if (features.Length == 0)
                throw new ArgumentException("Features cannot be empty.");

            Features = features;
            Gender = gender;
        }
    }
}