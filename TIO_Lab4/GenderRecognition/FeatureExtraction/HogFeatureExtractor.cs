﻿using System.Collections.Generic;
using System.Linq;
using Accord.Imaging;
using GenderRecognition.Data;

namespace GenderRecognition.FeatureExtraction
{
    public class HogFeatureExtractor : IFeatureExtractor
    {
        private readonly HistogramsOfOrientedGradients hog;

        public HogFeatureExtractor(int numberOfBins = 9, int blockSize = 3, int cellSize = 6)
        {
            hog = new HistogramsOfOrientedGradients(numberOfBins, blockSize, cellSize);
        }

        public List<FeatureData> ExtractFeatures(IEnumerable<ImageData> imagesData)
        {
            return imagesData
                .Select(ExtractFeaturesFromSingleImageData)
                .ToList();
        }

        private FeatureData ExtractFeaturesFromSingleImageData(ImageData imageData)
        {
            var featuresNestedList = hog.Transform(imageData.Bitmap)
                .Select(feature => feature.Descriptor.ToList())
                .ToList();

            var featuresArray = ToConcatenatedArray(featuresNestedList);

            return new FeatureData(featuresArray, imageData.Gender);
        }

        private static T[] ToConcatenatedArray<T>(IEnumerable<List<T>> nestedList)
        {
            return nestedList
                .SelectMany(innerList => 
                    innerList.ToList())
                .ToArray();
        }
    }
}