﻿using System.Collections.Generic;
using GenderRecognition.Data;

namespace GenderRecognition.FeatureExtraction
{
    public interface IFeatureExtractor
    {
        List<FeatureData> ExtractFeatures(IEnumerable<ImageData> imagesData);
    }
}