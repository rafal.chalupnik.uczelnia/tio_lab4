﻿using System;
using System.Collections.Generic;
using System.Linq;
using Accord.Imaging.Filters;
using Accord.Neuro;
using Accord.Neuro.ActivationFunctions;
using Accord.Neuro.Learning;
using Accord.Neuro.Networks;
using GenderRecognition.Data;
using GenderRecognition.FeatureExtraction;

namespace GenderRecognition.NeuralNetwork
{
    public class GenderRecognitionNeuralNetwork : IGenderRecognitionNeuralNetwork
    {
        private readonly ActivationNetwork neuralNetwork;

        public GenderRecognitionNeuralNetwork()
        {
            neuralNetwork = new ActivationNetwork(new BipolarSigmoidFunction(2), 9801, 500, 1);
        }

        public void Train(List<FeatureData> trainingData, List<FeatureData> testData, double targetAccuracy)
        {
            var neuralNetworkLearning = new BackPropagationLearning(neuralNetwork)
            {
                LearningRate = 1.0
            };

            var actualAccuracy = 0.0;
            var currentError = double.MaxValue;

            while (actualAccuracy < targetAccuracy)
            {
                var batch = trainingData;

                var trainingInput = batch
                    .Select(data => data.Features)
                    .ToArray();

                var trainingOutput = batch
                    .Select(data => data.Gender)
                    .Select(GenderToOutputArray)
                    .ToArray();

                var epochError = neuralNetworkLearning.RunEpoch(trainingInput, trainingOutput);

                Console.WriteLine("");
                Console.WriteLine($"Epoch error: {epochError:##.######}");
                actualAccuracy = Evaluate(testData);

                Console.WriteLine($"Epoch accuracy: {actualAccuracy}");

                //if (epochError >= currentError)
                //{
                //    neuralNetworkLearning.LearningRate /= 10;
                //    Console.WriteLine($"Decreasing learning rate to {neuralNetworkLearning.LearningRate}");
                //}

                currentError = epochError;
            }
        }

        private double Evaluate(IReadOnlyCollection<FeatureData> testData)
        {
            var desiredOutput = testData.Select(data => data.Gender).ToList();
            var actualOutput = Process(testData.Select(data => data.Features));

            var correctGuesses = desiredOutput
                .Where((t, i) => t == actualOutput[i])
                .Count();

            return (double) correctGuesses / desiredOutput.Count;
        }

        private List<Gender> Process(IEnumerable<double[]> features)
        {
            return features
                .Select(feature => neuralNetwork.Compute(feature))
                .Select(OutputArrayToGender)
                .ToList();
        }

        private static double[] GenderToOutputArray(Gender gender)
        {
            switch (gender)
            {
                case Gender.Female: return new[] { 1.0 };
                case Gender.Male: return new[] { -1.0 };
                default: throw new NotSupportedException("Other genders are not supported.");
            }
        }

        private static Gender OutputArrayToGender(IReadOnlyList<double> outputArray)
        {
            return outputArray[0] > 0.0
                ? Gender.Female
                : Gender.Male;
        }

        private static List<T> SelectRandomly<T>(IEnumerable<T> from, int count)
        {
            var evaluatedFrom = from.ToList();
            var random = new Random();
            var output = new List<T>();

            for (var i = 0; i < count; i++)
            {
                var elem = evaluatedFrom[random.Next(evaluatedFrom.Count)];
                evaluatedFrom.Remove(elem);
                output.Add(elem);
            }

            return output;
        }
    }
}