﻿using System.Collections.Generic;
using GenderRecognition.FeatureExtraction;

namespace GenderRecognition.NeuralNetwork
{
    public interface IGenderRecognitionNeuralNetwork
    {
        void Train(List<FeatureData> trainingData, List<FeatureData> testData, double accuracy);
    }
}