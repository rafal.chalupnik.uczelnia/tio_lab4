﻿using GenderRecognition.Data.Loading;
using GenderRecognition.FeatureExtraction;
using System;
using System.Collections.Generic;
using System.Linq;
using GenderRecognition.NeuralNetwork;

namespace GenderRecognition
{
    class Program
    {
        static void Main(string[] args)
        {
            var loader = new DataLoader();
            var imagesData = loader.LoadImages(@"C:\Users\Raven\Desktop\faces10");
            Console.WriteLine("Loaded");

            var featureExtractor = new HogFeatureExtractor();
            var extractedFeatures = featureExtractor.ExtractFeatures(imagesData);
            Console.WriteLine("Extracted");

            //var testFeatures = SelectRandomly(extractedFeatures, 100);
            //var trainingFeatures = extractedFeatures
            //    .Except(testFeatures)
            //    .ToList();

            var neuralNetwork = new GenderRecognitionNeuralNetwork();
            neuralNetwork.Train(extractedFeatures, extractedFeatures, 0.9);

            Console.WriteLine("Done!");
            Console.ReadLine();
        }

        private static List<T> SelectRandomly<T>(IEnumerable<T> from, int count)
        {
            var evaluatedFrom = from.ToList();
            var random = new Random();
            var output = new List<T>();

            for (var i = 0; i < count; i++)
            {
                var elem = evaluatedFrom[random.Next(evaluatedFrom.Count)];
                evaluatedFrom.Remove(elem);
                output.Add(elem);
            }

            return output;
        }
    }
}
